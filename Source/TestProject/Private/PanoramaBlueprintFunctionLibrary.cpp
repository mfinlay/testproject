// Fill out your copyright notice in the Description page of Project Settings.

#include "PanoramaBlueprintFunctionLibrary.h"




void UPanoramaBlueprintFunctionLibrary::CapturePanorama(const FVector LocationOfPoint, const FString SaveDirectory, const FString FileName, const int32 Resolution, const ELosslessFileFormat FileType, const ERenderType Type, bool &Success, FString &OutputPath)
{

	if (!FPaths::DirectoryExists(SaveDirectory))
	{
		//Directory Does Not Exist
		Success = false;
	}
	else
	{
		if (Type == ERenderType::E_Equirectangular)
		{

			//Create a cube Render target 
			 //UTextureRenderTargetCube

			//Set the width of the cube render target(in pixels);

			//Create a scene capture cube

			//set the cube to use the render target and activate once(only when moved).

			//Export the texture from the render target

		}
		else if (Type == ERenderType::E_Cubemap)
		{

			UTextureRenderTarget2D* Tex2D = CreateTextureRenderTarget2D(Resolution, Resolution, FLinearColor(0, 0, 0, 0));
			//Create a 2D RenderTarget  

			//Create a 2D
			//Rotate/Capture/Export 6 Images from the LocationOfPoint

			//Combine the images and
			//Position them as X,T,X  or also  00,+Z,00
			//				   L,F,R		   -Y,+X,+Y
			//				   X,B,X		   00,-Z,00
			//Rotating them so that seams are shared


		}
		else
		{
			//Unknown Type
			Success = false;
		}

		//Set output path and save based on File Format

		//OutputPath = FPaths::Combine();
	}

}



//this is how you can make cpp only internal functions!
static EImageFormat::Type GetJoyImageFormat(EJoyImageFormats JoyFormat)
{
	/*
	ImageWrapper.h
	namespace EImageFormat
	{

	Enumerates the types of image formats this class can handle

	enum Type
	{
	//Portable Network Graphics
	PNG,

	//Joint Photographic Experts Group
	JPEG,

	//Single channel jpeg
	GrayscaleJPEG,

	//Windows Bitmap
	BMP,

	//Windows Icon resource
	ICO,

	//OpenEXR (HDR) image file format
	EXR,

	//Mac icon
	ICNS
	};
	};
	*/
	switch (JoyFormat)
	{
	case EJoyImageFormats::JPG: return EImageFormat::JPEG;
	case EJoyImageFormats::PNG: return EImageFormat::PNG;
	case EJoyImageFormats::BMP: return EImageFormat::BMP;
	case EJoyImageFormats::ICO: return EImageFormat::ICO;
	case EJoyImageFormats::EXR: return EImageFormat::EXR;
	case EJoyImageFormats::ICNS: return EImageFormat::ICNS;
	}
	return EImageFormat::JPEG;
}

static FString GetJoyImageExtension(EJoyImageFormats JoyFormat)
{
	switch (JoyFormat)
	{
	case EJoyImageFormats::JPG: return ".jpg";
	case EJoyImageFormats::PNG: return ".png";
	case EJoyImageFormats::BMP: return ".bmp";
	case EJoyImageFormats::ICO: return ".ico";
	case EJoyImageFormats::EXR: return ".exr";
	case EJoyImageFormats::ICNS: return ".icns";
	}
	return ".png";
}



static TSharedPtr<IImageWrapper> GetImageWrapperByExtention(const FString InImagePath)
{
	IImageWrapperModule& ImageWrapperModule = FModuleManager::LoadModuleChecked<IImageWrapperModule>(FName("ImageWrapper"));
	if (InImagePath.EndsWith(".png"))
	{
		return ImageWrapperModule.CreateImageWrapper(EImageFormat::PNG);
	}
	else if (InImagePath.EndsWith(".jpg") || InImagePath.EndsWith(".jpeg"))
	{
		return ImageWrapperModule.CreateImageWrapper(EImageFormat::JPEG);
	}
	else if (InImagePath.EndsWith(".bmp"))
	{
		return ImageWrapperModule.CreateImageWrapper(EImageFormat::BMP);
	}
	else if (InImagePath.EndsWith(".ico"))
	{
		return ImageWrapperModule.CreateImageWrapper(EImageFormat::ICO);
	}
	else if (InImagePath.EndsWith(".exr"))
	{
		return ImageWrapperModule.CreateImageWrapper(EImageFormat::EXR);
	}
	else if (InImagePath.EndsWith(".icns"))
	{
		return ImageWrapperModule.CreateImageWrapper(EImageFormat::ICNS);
	}

	return nullptr;
}


UTextureRenderTarget2D* UPanoramaBlueprintFunctionLibrary::CreateTextureRenderTarget2D(int32 InSizeX, int32 InSizeY, FLinearColor ClearColor)
{
	UTextureRenderTarget2D* RenderTarget = NewObject<UTextureRenderTarget2D>();

	if (RenderTarget)
	{
		RenderTarget->AddToRoot();
		RenderTarget->ClearColor = ClearColor;
		RenderTarget->InitAutoFormat((InSizeX == 0) ? 256 : InSizeX, (InSizeY == 0) ? 256 : InSizeY);
	}

	return RenderTarget;
}

bool UPanoramaBlueprintFunctionLibrary::CaptureComponent2D_Project(class USceneCaptureComponent2D* Target, FVector Location, FVector2D& OutPixelLocation)
{
	if ((Target == nullptr) || (Target->TextureTarget == nullptr))
	{
		return false;
	}

	const FTransform& Transform = Target->GetComponentToWorld();
	FMatrix ViewMatrix = Transform.ToInverseMatrixWithScale();
	FVector ViewLocation = Transform.GetTranslation();

	// swap axis st. x=z,y=x,z=y (unreal coord space) so that z is up
	ViewMatrix = ViewMatrix * FMatrix(
		FPlane(0, 0, 1, 0),
		FPlane(1, 0, 0, 0),
		FPlane(0, 1, 0, 0),
		FPlane(0, 0, 0, 1));

	const float FOV = Target->FOVAngle * (float)PI / 360.0f;

	FIntPoint CaptureSize(Target->TextureTarget->GetSurfaceWidth(), Target->TextureTarget->GetSurfaceHeight());

	float XAxisMultiplier;
	float YAxisMultiplier;

	if (CaptureSize.X > CaptureSize.Y)
	{
		// if the viewport is wider than it is tall
		XAxisMultiplier = 1.0f;
		YAxisMultiplier = CaptureSize.X / (float)CaptureSize.Y;
	}
	else
	{
		// if the viewport is taller than it is wide
		XAxisMultiplier = CaptureSize.Y / (float)CaptureSize.X;
		YAxisMultiplier = 1.0f;
	}

	FMatrix    ProjectionMatrix = FReversedZPerspectiveMatrix(
		FOV,
		FOV,
		XAxisMultiplier,
		YAxisMultiplier,
		GNearClippingPlane,
		GNearClippingPlane
	);

	FMatrix ViewProjectionMatrix = ViewMatrix * ProjectionMatrix;

	FVector4 ScreenPoint = ViewProjectionMatrix.TransformFVector4(FVector4(Location, 1));

	if (ScreenPoint.W > 0.0f)
	{
		float InvW = 1.0f / ScreenPoint.W;
		float Y = (GProjectionSignY > 0.0f) ? ScreenPoint.Y : 1.0f - ScreenPoint.Y;
		FIntRect ViewRect = FIntRect(0, 0, CaptureSize.X, CaptureSize.Y);
		OutPixelLocation = FVector2D(
			ViewRect.Min.X + (0.5f + ScreenPoint.X * 0.5f * InvW) * ViewRect.Width(),
			ViewRect.Min.Y + (0.5f - Y * 0.5f * InvW) * ViewRect.Height()
		);
		return true;
	}

	return false;
}

bool UPanoramaBlueprintFunctionLibrary::Capture2D_Project(class ASceneCapture2D* Target, FVector Location, FVector2D& OutPixelLocation)
{
	return (Target) ? CaptureComponent2D_Project(Target->GetCaptureComponent2D(), Location, OutPixelLocation) : false;
}

bool UPanoramaBlueprintFunctionLibrary::CaptureComponent2D_SaveImage(class USceneCaptureComponent2D* Target, const FString ImagePath, const FLinearColor ClearColour)
{
	// Bad scene capture component! No render target! Stay! Stay! Ok, feed!... wait, where was I?
	if ((Target == nullptr) || (Target->TextureTarget == nullptr))
	{
		return false;
	}

	FRenderTarget* RenderTarget = Target->TextureTarget->GameThread_GetRenderTargetResource();
	if (RenderTarget == nullptr)
	{
		return false;
	}

	TArray<FColor> RawPixels;

	// Format not supported - use PF_B8G8R8A8.
	if (Target->TextureTarget->GetFormat() != PF_B8G8R8A8)
	{
		// TRACEWARN("Format not supported - use PF_B8G8R8A8.");
		return false;
	}

	if (!RenderTarget->ReadPixels(RawPixels))
	{
		return false;
	}

	// Convert to FColor.
	FColor ClearFColour = ClearColour.ToFColor(false); // FIXME - want sRGB or not?

	for (auto& Pixel : RawPixels)
	{
		// Switch Red/Blue changes.
		const uint8 PR = Pixel.R;
		const uint8 PB = Pixel.B;
		Pixel.R = PB;
		Pixel.B = PR;

		// Set alpha based on RGB values of ClearColour.
		Pixel.A = ((Pixel.R == ClearFColour.R) && (Pixel.R == ClearFColour.R) && (Pixel.R == ClearFColour.R)) ? 0 : 255;
	}
	//IImageWrapperPtr ImageWrapper = GetImageWrapperByExtention(InImagePath);
	TSharedPtr<IImageWrapper> ImageWrapper = GetImageWrapperByExtention(ImagePath);
	//IImageWrapperPtr ImageWrapper = GetImageWrapperByExtention(ImagePath);

	const int32 Width = Target->TextureTarget->SizeX;
	const int32 Height = Target->TextureTarget->SizeY;

	if (ImageWrapper.IsValid() && ImageWrapper->SetRaw(&RawPixels[0], RawPixels.Num() * sizeof(FColor), Width, Height, ERGBFormat::RGBA, 8))
	{
		FFileHelper::SaveArrayToFile(ImageWrapper->GetCompressed(), *ImagePath);
		return true;
	}

	return false;
}

bool UPanoramaBlueprintFunctionLibrary::Capture2D_SaveImage(class ASceneCapture2D* Target, const FString ImagePath, const FLinearColor ClearColour)
{
	return (Target) ? CaptureComponent2D_SaveImage(Target->GetCaptureComponent2D(), ImagePath, ClearColour) : false;
}


UTexture2D* UPanoramaBlueprintFunctionLibrary::LoadTexture2D_FromFileByExtension(const FString& ImagePath, bool& IsValid, int32& OutWidth, int32& OutHeight)
{
	UTexture2D* Texture = nullptr;
	IsValid = false;

	// To avoid log spam, make sure it exists before doing anything else.
	if (!FPlatformFileManager::Get().GetPlatformFile().FileExists(*ImagePath))
	{
		return nullptr;
	}

	TArray<uint8> CompressedData;
	if (!FFileHelper::LoadFileToArray(CompressedData, *ImagePath))
	{
		return nullptr;
	}

	TSharedPtr<IImageWrapper> ImageWrapper = GetImageWrapperByExtention(ImagePath);
	//IImageWrapperPtr ImageWrapper = GetImageWrapperByExtention(ImagePath);

	if (ImageWrapper.IsValid() && ImageWrapper->SetCompressed(CompressedData.GetData(), CompressedData.Num()))
	{
		const TArray<uint8>* UncompressedRGBA = nullptr;

		if (ImageWrapper->GetRaw(ERGBFormat::RGBA, 8, UncompressedRGBA))
		{
			Texture = UTexture2D::CreateTransient(ImageWrapper->GetWidth(), ImageWrapper->GetHeight(), PF_R8G8B8A8);

			if (Texture != nullptr)
			{
				IsValid = true;

				OutWidth = ImageWrapper->GetWidth();
				OutHeight = ImageWrapper->GetHeight();

				void* TextureData = Texture->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
				FMemory::Memcpy(TextureData, UncompressedRGBA->GetData(), UncompressedRGBA->Num());
				Texture->PlatformData->Mips[0].BulkData.Unlock();
				Texture->UpdateResource();
			}
		}
	}

	return Texture;
}
