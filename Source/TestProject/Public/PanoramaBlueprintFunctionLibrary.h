// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//Texture2D
#include "Engine/TextureRenderTarget2D.h"
#include "Engine/Texture2D.h"
#include "Engine/SceneCapture.h"
#include "Engine/SceneCapture2D.h"
#include "Components/SceneCaptureComponent2D.h"
#include "Engine/SceneCaptureCube.h"
#include "Engine/TextureRenderTargetCube.h"
#include "PlatformFileManager.h"
//Kris Nodes
#include "ImageUtils.h"
//#include "Runtime/ImageWrapper/Public/Interfaces/IImageWrapperModule.h"
//#include "ImageWrapper.h"	//requires "ImageWrapper" in public dependencies in build CS
#include "IImageWrapperModule.h"

#include "IImageWrapper.h"
//#include "ImageWrapper.h"
#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Camera/CameraActor.h"
#include "PanoramaBlueprintFunctionLibrary.generated.h"


/*
Brief:
	Create a UE4 blueprint node in C++ that will enable designers to capture and save 360 degree panoramic renderings of a scene from a point in space.
Requirements:
	The blueprint node should have the following pins exposed to the designers:
		Location of point
		File name
		File format
		Save location
		Render Type (Equirectangular or cubemap)
		Render resolution
	The node should have the following output pins:
		Success
		File path (Save location + Name + Format)
	The output should include post processing effects.
	If the image outputs are not actually equirectangular but are still suitable to be processed through a 3rd party app such as Autopano to create the equirectangular projections, this is still acceptable.
	Your code should be written in a way that allows another programmer to evaluate your implementation.
*/


//A Selection of Lossless file formats
UENUM(BlueprintType)
enum class ELosslessFileFormat : uint8
{
	E_PNG 	UMETA(DisplayName = "PNG"),
	E_TIF 	UMETA(DisplayName = "Tiff")
};


//A Selection of Types that represent the output styles of the PanoramicRenderer
UENUM(BlueprintType)
enum class ERenderType : uint8
{
	E_Cubemap 	UMETA(DisplayName = "Cubemap"),
	E_Equirectangular UMETA(DisplayName = "Equirectangular")
};


UCLASS()
class TESTPROJECT_API UPanoramaBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	/* 
	Methods sourced from VictoryBPFunction by Rama
	Code Contributed by Community Member Kris!
	*/

	UFUNCTION(Category = "Panorama|Texture", BlueprintCallable)
		static class UTextureRenderTarget2D* CreateTextureRenderTarget2D(int32 InSizeX = 256, int32 InSizeY = 256, FLinearColor ClearColor = FLinearColor::Transparent);

	UFUNCTION(Category = "Panorama|SceneCapture", BlueprintPure)
		static bool CaptureComponent2D_Project(class USceneCaptureComponent2D* Target, FVector Location, FVector2D& OutPixelLocation);

	UFUNCTION(Category = "Panorama|SceneCapture", BlueprintPure, Meta = (DefaultToSelf = "Target"))
		static bool Capture2D_Project(class ASceneCapture2D* Target, FVector Location, FVector2D& OutPixelLocation);

	/** Make sure to include the appropriate image extension in your file path! Recommended: .bmp .jpg .png.*/
	UFUNCTION(Category = "Panorama|SceneCapture", BlueprintCallable)
		static bool CaptureComponent2D_SaveImage(class USceneCaptureComponent2D* Target, const FString ImagePath, const FLinearColor ClearColour);

	/** Make sure to include the appropriate image extension in your file path! Recommended: .bmp, .jpg, .png. */
	UFUNCTION(Category = "Panorama|SceneCapture", BlueprintCallable, Meta = (DefaultToSelf = "Target"))
		static bool Capture2D_SaveImage(class ASceneCapture2D* Target, const FString ImagePath, const FLinearColor ClearColour);

	/** Make sure your image path has a valid extension! */
	UFUNCTION(Category = "Panorama|Load Texture From File", BlueprintCallable)
		static UTexture2D*  LoadTexture2D_FromFileByExtension(const FString& ImagePath, bool& IsValid, int32& OutWidth, int32& OutHeight);

	/* End Contributions by Kris*/





	//Exports a panorama using the provided resolution. Resolution Value more accurately refers to width in pixels.
	UFUNCTION(BlueprintCallable, Category = "Panorama")
		static void CapturePanorama(const FVector LocationOfPoint, const FString SaveDirectory, const FString FileName, const int32 Resolution, const ELosslessFileFormat FileType, const ERenderType Type, bool &Success, FString &OutputPath);

};
